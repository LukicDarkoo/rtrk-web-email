import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, browserHistory, IndexRoute} from 'react-router'
import App from './components/app/App';
import UserLogin from './components/user/UserLogin';
import NoMatch from './components/nomatch/NoMatch';
import Welcome from './components/welcome/Welcome';
import UserRegister from './components/user/UserRegister';
import EmailNew from './components/email/EmailSend';
import EmailList from './components/email/EmailList';
import EmailView from './components/email/EmailView';
import Upload from './components/app/Upload';

import './index.css';
import 'bootstrap/dist/css/bootstrap.css';


ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Welcome}/>" +
            <Route path="upload" component={Upload}/>
            <Route path="user/login" component={UserLogin}/>
            <Route path="user/register" component={UserRegister}/>
            <Route path="email/new" component={EmailNew}/>
            <Route path="email/list" component={EmailList}/>
            <Route path="email/view/:emailId" component={EmailView}/>
            <Route path="*" component={NoMatch}/>
        </Route>
    </Router>,
    document.getElementById('root')
);


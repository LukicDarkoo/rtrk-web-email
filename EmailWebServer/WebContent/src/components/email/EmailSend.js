import React, {Component} from 'react';
import Rest from '../../Rest';
import EmailStore from '../../stores/EmailStore';
import {browserHistory} from 'react-router';

class EmailSend extends Component {
    constructor(props) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEmailSendError = this.onEmailSendError.bind(this);
        this.onEmailSendSuccess = this.onEmailSendSuccess.bind(this);

        this.state = {
            emailError: ''
        };
        this.nodes = {};
    }

    componentDidMount() {
        EmailStore.get().on(EmailStore.ACTION_MESSAGE_SEND_ERROR, this.onEmailSendError);
        EmailStore.get().on(EmailStore.ACTION_MESSAGE_SEND_SUCCESS, this.onEmailSendSuccess);
    }

    onEmailSendError(response) {
        let emailError = (<div className="alert alert-danger" role="alert">
                <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            {response.payload}
        </div>);
        this.setState({emailError: emailError});
    }

    onEmailSendSuccess(response) {
        let emailError = (
            <div className="alert alert-success" role="alert">
                <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                {response.payload}
            </div>
            );
        this.setState({emailError:emailError});

        setTimeout(() => {
            browserHistory.push('/email/list');
        }, 4000);
    }

    handleSubmit(e) {
        e.preventDefault();

        let params = {
            to: this.nodes.toInput.value,
            subject: this.nodes.subjectInput.value,
            content: this.nodes.contentInput.value
        };
        Rest.post('/email/send', params)
            .then(EmailStore.get().setEmailSendByResponse);
    }

    render() {
        return (<div className="EmailSend">
            <form onSubmit={this.handleSubmit}>
                <h2>Compose email</h2>

                {this.state.emailError}

                <div className="form-group">
                    <label htmlFor="inputUsername">Username</label>
                    <input type="text"
                           className="form-control"
                           id="inputUsername"
                           placeholder="To..."
                           ref={(input) => this.nodes.toInput = input}/>
                </div>

                <div className="form-group">
                    <label htmlFor="inputSubject">Subject</label>
                    <input type="text" className="form-control" id="inputSubject" placeholder="Subject"
                           ref={(input) => this.nodes.subjectInput = input}/>
                </div>

                <div className="form-group">
                    <label htmlFor="inputContent">Content</label>
                    <textarea
                        className="form-control"
                        rows="3"
                        id="inputContent"
                        placeholder="Content..."
                        ref={(input) => this.nodes.contentInput = input}>
                    </textarea>
                </div>

                <div className="form-group row">
                    <div className="offset-sm-2 col-sm-10">
                        <button type="submit" className="btn btn-primary">Send</button>
                    </div>
                </div>

            </form>
        </div>);
    }
}

export default EmailSend;
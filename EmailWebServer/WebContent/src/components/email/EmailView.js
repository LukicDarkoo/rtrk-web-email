import React, {Component} from 'react';
import Rest from '../../Rest';
import EmailStore from '../../stores/EmailStore';
import {Link} from 'react-router';
import './EmailView.css';


class EmailView extends Component {
    constructor(props) {
        super(props);

        this.state = {
            from: '...',
            to: '...',
            subject: '...',
            content: '...'
        };

        this.onNewEmailReady = this.onNewEmailReady.bind(this);
    }

    onNewEmailReady(email) {
        if (+email.id === +this.props.params.emailId) {
            this.setState({
                from: email.from,
                to: email.to,
                subject: email.subject,
                content: email.content
            });
        }
    }

    componentDidMount() {
        EmailStore.get().on(EmailStore.ACTION_NEW_EMAIL_READY, this.onNewEmailReady);

        Rest.get('/email/receive', {id: this.props.params.emailId})
            .then(EmailStore.get().setEmailReceiveByResponse);
    }

    render() {
        return (<div className="EmailView">
            <table>
                <tbody>
                    <tr>
                        <td>From: </td>
                        <th>{this.state.from}</th>
                    </tr>
                    <tr>
                        <td>To: </td>
                        <th>{this.state.to}</th>
                    </tr>
                    <tr>
                        <td>Subject: </td>
                        <th>{this.state.subject}</th>
                    </tr>
                    <tr>
                        <td>Content: </td>
                        <th>{this.state.content}</th>
                    </tr>
                    <tr>
                        <td colSpan="2">
                            <Link to={`email/list`}>Back</Link>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>);
    }
}

export default EmailView;
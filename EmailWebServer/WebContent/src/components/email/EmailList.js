import React, {Component} from 'react';
import Rest from '../../Rest';
import EmailStore from '../../stores/EmailStore';
import {Link} from 'react-router';


class EmailList extends Component {
    constructor(props) {
        super(props);

        this.onEmailsReady = this.onEmailsReady.bind(this);

        this.state = {
            emails: ''
        };
    }

    componentDidMount() {
        EmailStore.get().on(EmailStore.ACTION_EMAILS_READY, this.onEmailsReady);

        Rest.get('/email/list')
            .then(EmailStore.get().setEmailListByResponse);
    }

    onEmailsReady(emails) {
        let emailHtmls = [];
        for (let email of emails) {
            emailHtmls.push(<tr key={'email-' + email.id}>
                <td>{email.from}</td>
                <td>
                    <Link to={`/email/view/` + email.id}>{email.subject}</Link>
                </td>
            </tr>);
        }
        this.setState({
            emails: (<tbody>{emailHtmls}</tbody>)
        });
    }

    render() {
        return (<div className="Email-List">
            <table className="table table-striped">
                <thead>
                <tr>
                    <th>From</th>
                    <th>Subject</th>
                </tr>
                </thead>
                {this.state.emails}
            </table>
        </div>);
    }
}

export default EmailList;
import React, {Component} from 'react';
import $ from 'jquery';


class Upload extends Component {
	constructor(params) {
		super(params);
		
		this.handleSubmit = this.handleSubmit.bind(this);
		
		this.nodes = {};
	}
	
    componentDidMount() {

    }
    
    handleSubmit(e) {
    	e.preventDefault();
    	
    	let files = this.nodes.picture.files;
    	
    	console.log(files);
    	
    	// Reference: https://abandon.ie/notebook/simple-file-uploads-using-jquery-ajax
    	let data = new FormData();
        data.append("picture", files[0]);
 
        
        console.log(data);
        console.log('v1');

        $.ajax({
            url: 'http://localhost:8080/EmailWebServer/rest/upload/picture',
            type: 'POST',
            data: data,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function(data, textStatus, jqXHR)
            {
                if(typeof data.error === 'undefined')
                {
                    // Success so call function to process the form
                    // submitForm(event, data);
                }
                else
                {
                    // Handle errors here
                    console.log('ERRORS: ' + data.error);
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                // Handle errors here
                console.log('ERRORS: ' + textStatus);
                // STOP LOADING SPINNER
            }
        });
    }

    render() {
        return (<div>
            <form onSubmit={this.handleSubmit} encType='multipart/form-data'>
            	<input type="file" name="picture" ref={(input) => this.nodes.picture = input} />
            	<button>Submit</button>
        	</form>
        </div>);
    }
}

export default Upload;
import React, {Component} from 'react';
import {Link, browserHistory} from 'react-router';
import './App.css';
import UserStore from '../../stores/UserStore';
import Rest from '../../Rest';


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            linksNode: ''
        };

        this.onLoginStatusChanged = this.onLoginStatusChanged.bind(this);
        this.onLogoffClicked = this.onLogoffClicked.bind(this);
    }

    componentDidMount() {
        UserStore.get().on(UserStore.ACTION_LOGIN_SUCCESS, this.onLoginStatusChanged);
        UserStore.get().on(UserStore.ACTION_LOGOFF_SUCCESS, this.onLoginStatusChanged);

        Rest.post('/user/login', {username: 'x', password: 'x'})
            .then(UserStore.get().setLoginStatusByResponse);
        this.onLoginStatusChanged();
    }

    onLogoffClicked(e) {
        e.preventDefault();
        Rest.post('/user/logoff', { execute: true })
            .then(UserStore.get().setLogoffStatusByResponse);
    }

    onLoginStatusChanged(response) {
        let linksNode = null;
        if (UserStore.get().isLogged() === true) {
            // Set menu
            linksNode = (
                <ul className="nav navbar-nav">
                    <li><Link to={`/email/new`}>New email</Link></li>
                    <li><a onClick={this.onLogoffClicked} href="#">Logoff</a></li>
                </ul>
            );
            // Redirect
            browserHistory.push('/email/list');
        } else {
            // Set menu
            linksNode = (
                <ul className="nav navbar-nav">
                    <li><Link to={`/`}>Home</Link></li>
                    <li><Link to={`/user/login`}>Login</Link></li>
                    <li><Link to={`/user/register`}>Register</Link></li>
                    <li><Link to={`/upload`}>Upload</Link></li>
                </ul>
            );
            // Redirect
            browserHistory.push('/user/login');
        }
        this.setState({linksNode : linksNode});
    }

    render() {
        return (
            <div className="container">
                <nav className="navbar navbar-inverse navbar-fixed-top">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"> </span>
                                <span className="icon-bar"> </span>
                                <span className="icon-bar"> </span>
                            </button>
                            <a className="navbar-brand" href="#">LukicDarkoo's Email</a>
                        </div>
                        <div id="navbar" className="navbar-collapse collapse">
                            {this.state.linksNode}
                        </div>
                    </div>
                </nav>

                <div className="content App-content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default App;

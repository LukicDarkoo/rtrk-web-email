import React, {Component} from 'react';
import './Welcome.css';

class Welcome extends Component {
    render() {
        return (
            <div className="Welcome">
                <h2>Welcome page</h2>
            </div>
        );
    }
}

export default Welcome;

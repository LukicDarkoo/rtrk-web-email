import React, {Component} from 'react';
import Rest from '../../Rest';
import UserStore from '../../stores/UserStore';

class UserLogin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loginError: ''
        };

        this.nodes = {};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onLoginError = this.onLoginError.bind(this);

        UserStore.get().on(UserStore.ACTION_LOGIN_ERROR, this.onLoginError);
    }

    onLoginError(response) {
        let loginError = (<div className="alert alert-danger" role="alert">
                <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    {response.payload}
                </div>);
        this.setState({loginError: loginError});
    }

    handleSubmit(event) {
        event.preventDefault();

        let params = {
            username: this.nodes.usernameInput.value,
            password: this.nodes.passwordInput.value
        };
        Rest.post('/user/login', params)
            .then(UserStore.get().setLoginStatusByResponse);
    }

    render() {
        return (
            <div className="Login col-md-5 col-md-offset-3">
                <form onSubmit={this.handleSubmit}>
                    <h2>Login</h2>

                    {this.state.loginError}

                    <div className="form-group">
                        <label htmlFor="inputUsername">Username</label>
                        <input type="username"
                               className="form-control"
                               id="inputUsername"
                               placeholder="Username..."
                               ref={(input) => this.nodes.usernameInput = input}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputPassword">Password</label>
                        <input type="password" className="form-control" id="inputPassword" placeholder="Password"
                               ref={(input) => this.nodes.passwordInput = input}/>
                    </div>

                    <div className="form-group row">
                        <div className="offset-sm-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Sign in</button>
                        </div>
                    </div>

                </form>
            </div>
        );
    }
}

export default UserLogin;

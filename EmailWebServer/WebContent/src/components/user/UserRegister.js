import React, {Component} from 'react';
import Rest from '../../Rest';
import UserStore from '../../stores/UserStore';

class UserRegister extends Component {
    constructor(props) {
        super(props);

        this.state = {
            registerError: ''
        };

        this.nodes = {};

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRegisterError = this.onRegisterError.bind(this);

        UserStore.get().on(UserStore.ACTION_REGISTER_ERROR, this.onRegisterError);
    }

    onRegisterError(response) {
        let registerError = (<div className="alert alert-danger" role="alert">
            <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
            {response.payload}
        </div>);
        this.setState({registerError: registerError});
    }

    handleSubmit(event) {
        event.preventDefault();

        let params = {
            username: this.nodes.usernameInput.value,
            password: this.nodes.passwordInput.value,
            name: this.nodes.nameInput.value
        };
        Rest.post('/user/register', params)
            .then(UserStore.get().setRegisterStatusByResponse);
    }

    render() {
        return (
            <div className="Register col-md-5 col-md-offset-3">
                <form onSubmit={this.handleSubmit}>
                    <h2>Register</h2>

                    {this.state.registerError}

                    <div className="form-group">
                        <label htmlFor="inputUsername">Username</label>
                        <input type="username"
                               className="form-control"
                               id="inputUsername"
                               placeholder="Username..."
                               ref={(input) => this.nodes.usernameInput = input}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputPassword">Password</label>
                        <input type="password" className="form-control" id="inputPassword" placeholder="Password"
                               ref={(input) => this.nodes.passwordInput = input}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="inputName">Name</label>
                        <input type="username"
                               className="form-control"
                               id="inputName"
                               placeholder="Name..."
                               ref={(input) => this.nodes.nameInput = input}/>
                    </div>

                    <div className="form-group row">
                        <div className="offset-sm-2 col-sm-10">
                            <button type="submit" className="btn btn-primary">Register</button>
                        </div>
                    </div>

                </form>
            </div>
        );
    }
}

export default UserRegister;

import React, {Component} from 'react';

class NoMatch extends Component {
    render() {
        return (
            <div className="NoMatch">
                <h1>Hey! Where are you? :O</h1>
            </div>
        );
    }
}

export default NoMatch;

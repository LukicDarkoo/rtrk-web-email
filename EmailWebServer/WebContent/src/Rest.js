import $ from 'jquery';

class Rest {
    static post(url, params) {
        return Rest._do(url, params, 'POST');
    }

    static get(url, params) {
        return Rest._do(url, params, 'GET');
    }

    static put(url, params) {
        return Rest._do(url, params, 'PUT');
    }

    static _do(url, params, method) {
        return new Promise((resolve, reject) => {
            $.ajax({
                dataType: 'json',
                method: method,
                xhrFields: {
                    withCredentials: true
                },
                url: 'http://localhost:8080/EmailWebServer/rest' + url,
                data: params,
                success: resolve,
                error: reject
            });
        });
    }
}

export default Rest;
import { EventEmitter } from 'events';

let instance = null;

class UserStore extends EventEmitter {
    static get ACTION_LOGIN_SUCCESS() { return 'ACTION_LOGIN_SUCCESS'; }
    static get ACTION_LOGIN_ERROR() { return 'ACTION_LOGIN_ERROR'; };
    static get ACTION_LOGOFF_SUCCESS() { return 'ACTION_LOGOFF_SUCCESS'; }
    static get ACTION_REGISTER_SUCCESS() { return 'ACTION_REGISTER_SUCCESS'; }
    static get ACTION_REGISTER_ERROR() { return 'ACTION_REGISTER_ERROR'; }

    static get() {
        if (instance === null) {
            instance = new UserStore();
        }
        return instance;
    }

    constructor() {
        super();

        this.logged = false;

        this.setLoginStatusByResponse = this.setLoginStatusByResponse.bind(this);
        this.setLogoffStatusByResponse = this.setLogoffStatusByResponse.bind(this);
        this.setRegisterStatusByResponse = this.setRegisterStatusByResponse.bind(this);
        this.isLogged = this.isLogged.bind(this);
    }

    setLoginStatusByResponse(response) {
        if (response.code === 'SUCCESS' || response.code === 'ALREADY_LOGGED_IN') {
            this.logged = true;
            this.emit(UserStore.ACTION_LOGIN_SUCCESS, response);
        } else {
            this.emit(UserStore.ACTION_LOGIN_ERROR, response);
        }
    }

    setRegisterStatusByResponse(response) {
        if (response.code === 'SUCCESS') {
            this.logged = true;
            this.emit(UserStore.ACTION_REGISTER_SUCCESS, response);
            this.emit(UserStore.ACTION_LOGIN_SUCCESS, response);
        } else {
            this.emit(UserStore.ACTION_REGISTER_ERROR, response);
        }
    }

    setLogoffStatusByResponse(response) {
        this.logged = false;
        this.emit(UserStore.ACTION_LOGOFF_SUCCESS, response);
    }

    isLogged() {
        return this.logged;
    }
}

export default UserStore;

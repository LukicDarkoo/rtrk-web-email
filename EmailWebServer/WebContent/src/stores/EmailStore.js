import { EventEmitter } from 'events';

let instance = null;

class EmailStore extends EventEmitter {
    static get ACTION_EMAILS_READY() { return 'ACTION_EMAILS_READY'; }
    static get ACTION_NEW_EMAIL_READY() { return 'ACTION_NEW_EMAIL_READY'; }
    static get ACTION_MESSAGE_SEND_ERROR() { return 'ACTION_MESSAGE_SEND_ERROR'; }
    static get ACTION_MESSAGE_SEND_SUCCESS() { return 'ACTION_MESSAGE_SEND_SUCCESS'; }

    static get() {
        if (instance === null) {
            instance = new EmailStore();
        }
        return instance;
    }

    constructor() {
        super();

        this.setEmailSendByResponse = this.setEmailSendByResponse.bind(this);
        this.setEmailListByResponse = this.setEmailListByResponse.bind(this);
        this.setEmailReceiveByResponse = this.setEmailReceiveByResponse.bind(this);
        this.getEmails = this.getEmails.bind(this);

        this.emails = [];
    }

    setEmailReceiveByResponse(response) {
        if (response.code === 'SUCCESS') {
            let email = JSON.parse(response.payload);
            this.emit(EmailStore.ACTION_NEW_EMAIL_READY, email);
        }
    }

    setEmailListByResponse(response) {
        if (response.code === 'SUCCESS') {
            this.emails = JSON.parse(response.payload);
            this.emit(EmailStore.ACTION_EMAILS_READY, this.emails);
        }
    }

    setEmailSendByResponse(response) {
        if (response.code === 'SUCCESS') {
            this.emit(EmailStore.ACTION_MESSAGE_SEND_SUCCESS, response);
        } else {
            this.emit(EmailStore.ACTION_MESSAGE_SEND_ERROR, response);
        }
    }

    getEmails() {
        return this.emails;
    }
}

export default EmailStore;

package darko.lukic.emailwebserver.dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import darko.lukic.emailshared.Config;
import darko.lukic.emailshared.models.Response;

/**
 * Provides user data from another server
 * @author lukicdarkoo
 *
 */
public class UserDAO {
	static private UserDAO instance = null;
	
	private InetAddress addr;
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out; 
	
	/**
	 * Get an instance of UserDAO
	 * @return
	 */
	public static UserDAO get() {
		if (instance == null) {
			instance = new UserDAO();
		}
		return instance;
	}
	
	/**
	 * Takes unique token from session and if there is no token it 
	 * generates a new one and put in session
	 * @param request HTML servlet request
	 * @return Generated token
	 */
	static public String getToken(HttpServletRequest request) {
		String token = (String)request.getSession().getAttribute("token");
		if (token == null) {
			token = UUID.randomUUID().toString();
			request.getSession().setAttribute("token", token);
		}
		System.out.println("Token: " + token);
		return token;
	}
	
	/**
	 * Sends command to server with token
	 * @param request HTML servlet request
	 * @param command Command
	 * @return Response on command
	 */
	static public String command(HttpServletRequest request, String command) {
		return UserDAO.get().sendCommand(getToken(request) + " " + command);
	}
	
	/**
	 * Sends command
	 * @param command Command
	 * @return Response on command
	 */
	public String sendCommand(String command) {
		String output = "";
		String line;
		
		try {
			out.println(command);
			
			while ((line = in.readLine()).equals("") == false) {
				output += line;
			}
			return output;
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	private UserDAO() {
		String hostname = "localhost";
		
		try {
			// Get address of server
			addr = InetAddress.getByName(hostname);

			// Open a socket to server
			socket = new Socket(addr, Config.SERVER2SERVER_PORT);

			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		
		// Close all streams
		in.close();
		out.close();
		socket.close();
	}
}

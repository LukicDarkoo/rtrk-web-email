package darko.lukic.emailwebserver.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import darko.lukic.emailwebserver.dao.UserDAO;

/**
 * Enables direct communication to another server
 * @author lukicdarkoo
 * 
 */
@Path("/command")
public class CommandController {
	@GET
	@Produces({"application/json"})
	public String command(@Context HttpServletRequest request) {
		return UserDAO.command(request, request.getParameter("command"));
	}
}

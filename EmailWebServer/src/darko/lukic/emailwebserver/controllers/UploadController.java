package darko.lukic.emailwebserver.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

@Path("/upload")
public class UploadController {
	// Reference: http://stackoverflow.com/a/25889454/1983050
	// We need new Maven dependency: https://mvnrepository.com/artifact/org.glassfish.jersey.media/jersey-media-multipart
	// Dependency injection problem: http://stackoverflow.com/a/25312655/1983050
	@POST
	@Path("/picture")  //Your Path or URL to call this service
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@Context ServletContext ctx,
			@FormDataParam("picture") InputStream uploadedInputStream,
	        @FormDataParam("picture") FormDataContentDisposition fileDetail) {
	     //Your local disk path where you want to store the file

		
		// I cannot believe it so hard to make relative path
		// Why is Java so retarded language >_< >_< >_<
		// "Java, I don't care about, what a horrible language", Linus Torvalds
	    String uploadedFileLocation = "/home/lukicdarkoo/workspace/EmailWebServer/WebContent/" + fileDetail.getFileName();
	    System.out.println(uploadedFileLocation);
	    // save it
	    File  objFile=new File(uploadedFileLocation);
	    if(objFile.exists())
	    {
	        objFile.delete();

	    }

	    saveToFile(uploadedInputStream, uploadedFileLocation);

	    String output = "File uploaded via Jersey based RESTFul Webservice to: " + uploadedFileLocation;

	    return Response.status(200).entity(output).build();

	}
	private void saveToFile(InputStream uploadedInputStream,
	        String uploadedFileLocation) {

	    try {
	        OutputStream out = null;
	        int read = 0;
	        byte[] bytes = new byte[1024];

	        out = new FileOutputStream(new File(uploadedFileLocation));
	        while ((read = uploadedInputStream.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	        out.flush();
	        out.close();
	    } catch (IOException e) {

	        e.printStackTrace();
	    }

	}
}

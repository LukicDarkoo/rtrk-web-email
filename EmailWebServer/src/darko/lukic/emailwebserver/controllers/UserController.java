package darko.lukic.emailwebserver.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import darko.lukic.emailwebserver.dao.UserDAO;

/**
 * User API
 * @author lukicdarkoo
 *
 */
@Path("/user")
public class UserController {
	@Path("/login")
	@POST
	@Produces({"application/json"})
	public String login(
			@Context HttpServletRequest request,
			@FormParam("username") String username,
			@FormParam("password") String password) {	
		return UserDAO.command(request, 
				"LOGIN " + 
				username + " " +
				password
		);
	}
	
	@Path("/register")
	@POST
	@Produces({"application/json"})
	public String register(
			@Context HttpServletRequest request,
			@FormParam("username") String username,
			@FormParam("password") String password,
			@FormParam("name") String name) {	
		return UserDAO.command(request, 
				"REGISTER " + 
				username + " " +
				password + " " + 
				name
		);
	}	
	
	@Path("/logoff")
	@POST
	@Produces({"application/json"})
	public String logoff(@Context HttpServletRequest request, @FormParam("execute") String execute) {	
		return UserDAO.command(request, 
				"LOGOFF"
		);
	}
}

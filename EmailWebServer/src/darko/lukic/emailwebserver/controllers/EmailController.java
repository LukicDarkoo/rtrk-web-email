package darko.lukic.emailwebserver.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import darko.lukic.emailwebserver.dao.UserDAO;

/**
 * Email API
 * @author lukicdarkoo
 *
 */
@Path("/email")
public class EmailController {
	@Path("/send")
	@POST
	@Produces({"application/json"})
	public String send(
			@Context HttpServletRequest request,
			@FormParam("to") String to,
			@FormParam("subject") String subject,
			@FormParam("content") String content) {	
		return UserDAO.command(request, 
				"SEND " +
				to + " " +
				"\"" + subject + "\" " +
				"\"" + content + "\""
		);
	}
	
	@Path("/list")
	@GET
	@Produces({"application/json"})
	public String list(
			@Context HttpServletRequest request) {	
		return UserDAO.command(request, 
				"LIST"
		);
	}
	
	@Path("/receive")
	@GET
	@Produces({"application/json"})
	public String receive(
			@Context HttpServletRequest request) {	
		return UserDAO.command(request, 
				"RECEIVE " + 
				request.getParameter("id")
		);
	}	
}

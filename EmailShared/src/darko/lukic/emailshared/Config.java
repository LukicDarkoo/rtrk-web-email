package darko.lukic.emailshared;

/**
 * Shared configuration parameters
 * @author lukicdarkoo
 *
 */
public class Config {
	/**
	 * Port which server uses to communicate with client
	 */
	public static final int SERVER2CLIENT_PORT = 9000;
	
	/**
	 * Port which server uses to communicate with server
	 */
	public static final int SERVER2SERVER_PORT = 9001;
}

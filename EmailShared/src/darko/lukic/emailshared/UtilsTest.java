package darko.lukic.emailshared;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilsTest {

	@Test
	public void testIsInteger() {
		assert(Utils.isInteger("c") == false);
		assert(Utils.isInteger("1.1") == false);
		assert(Utils.isInteger("1") == true);
	}

}

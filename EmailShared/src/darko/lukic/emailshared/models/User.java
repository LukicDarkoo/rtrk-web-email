package darko.lukic.emailshared.models;

/**
 * Model for user
 * @author lukicdarkoo
 *
 */
public class User {
	/**
	 * User's username
	 */
	private String username;
	
	/**
	 * User's password
	 */
	private String password;
	
	/**
	 * User's name
	 */
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}

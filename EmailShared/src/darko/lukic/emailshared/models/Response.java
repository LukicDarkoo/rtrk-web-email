package darko.lukic.emailshared.models;

/**
 * Model which will be sent to client as JSON stream
 * @author lukicdarkoo
 *
 */
public class Response {
	/**
	 * Code of reponse
	 */
	private ResponseCode code;
	
	/**
	 * Content of response
	 */
	private String payload;
	
	public Response(ResponseCode code, String payload) {
		super();
		this.code = code;
		this.payload = payload;
	}
	
	/**
	 * Get response code
	 * @return Response code
	 */
	public ResponseCode getCode() {
		return code;
	}
	
	/**
	 * Set response code
	 * @param code Response code
	 */
	public void setCode(ResponseCode code) {
		this.code = code;
	}
	
	/**
	 * Get response content
	 * @return Content
	 */
	public String getPayload() {
		return payload;
	}
	
	/**
	 * Set response content
	 * @param payload Response content
	 */
	public void setPayload(String payload) {
		this.payload = payload;
	}
}

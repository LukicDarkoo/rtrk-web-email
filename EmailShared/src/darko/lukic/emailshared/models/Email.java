package darko.lukic.emailshared.models;

import com.google.gson.annotations.Expose;

/**
 * Model for email
 * @author lukicdarkoo
 *
 */
public class Email {
	/**
	 * Username of sender
	 */
	@Expose private String from;
	
	/**
	 * Username of receiver
	 */
	private String to;
	
	/**
	 * Subject of email
	 */
	@Expose private String subject;
	
	/**
	 * Content of email
	 */
	private String content;
	
	/**
	 * ID of message
	 */
	@Expose private int id;
	
	/**
	 * Message ID getter
	 * @return ID of message
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Message ID getter
	 * @param id ID of message
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}

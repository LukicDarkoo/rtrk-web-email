package darko.lukic.emailshared;

/**
 * Additional features to Java
 * @author lukicdarkoo
 *
 */
public class Utils {
	/**
	 * Detects if string can be converted to integer
	 * @param s String that could be converted to integer
	 * @return True if string convertible to integer
	 */
	public static boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	    } catch(NumberFormatException e) { 
	        return false; 
	    } catch(NullPointerException e) {
	        return false;
	    }
	    return true;
	}
}

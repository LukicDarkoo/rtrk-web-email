package darko.lukic.emailclient;

import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import com.google.gson.Gson;

import darko.lukic.emailshared.Config;
import darko.lukic.emailshared.models.Response;

/**
 * Bootstraping class for EmailCient
 * @author lukicdarkoo
 *
 */
public class Client {
	private static Gson gson = new Gson();
	
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Scanner scr = new Scanner(System.in);
		String hostname = "localhost";
		String responseJson = null;
		String request = null;
		
		try {
			// Get address of server
			InetAddress addr = InetAddress.getByName(hostname);

			// Open a socket to server
			Socket sock = new Socket(addr, Config.SERVER2CLIENT_PORT);

			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sock.getOutputStream())), true);

			
			System.out.println("Enter command...");
			while (!(request = scr.nextLine()).equalsIgnoreCase("EXIT")) {
				out.println(request);
				
				while (!(responseJson = in.readLine()).equals("")) {
					Response response = gson.fromJson(responseJson, Response.class);
					System.out.println(response.getPayload());
				}
			}

			// Close all streams
			in.close();
			out.close();
			sock.close();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

}

SHARED_DEPS=./EmailShared/bin:./EmailShared/libs/gson-2.6.2.jar:./EmailShared/libs/apache-commons-codec-1.4.jar

BUILD_SERVER=ant -buildfile EmailServer/build.xml
BUILD_CLIENT=ant -buildfile EmailClient/build.xml
RUN_SERVER=java -cp "./EmailServer/bin:${SHARED_DEPS}" darko.lukic.emailserver.Server 
RUN_CLIENT=java -cp "./EmailClient/bin:${SHARED_DEPS}" darko.lukic.emailclient.Client

all:
	$(BUILD_SERVER)
	$(BUILD_CLIENT)

server:
	$(BUILD_SERVER)
	$(RUN_SERVER)

client:
	$(BUILD_CLIENT)
	$(RUN_CLIENT)
	

package darko.lukic.emailserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import darko.lukic.emailserver.controllers.UserController;
import darko.lukic.emailshared.models.Response;
import darko.lukic.emailshared.models.ResponseCode;

/**
 * Communicates to clients
 * @author lukicdarkoo
 *
 */
public class ServerToClientThread extends Thread {
	private Socket socket;
	private BufferedReader in;
	private PrintWriter out;
	private UserController user = new UserController();
	private Gson gson = new Gson();
	
	public ServerToClientThread(Socket socket) {
		this.socket = socket;
		try {
			// Initialize input stream
			in = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));

			// Initialize output stream
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream())), true);
			
			// Start thread
			start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void run() {
		String request = null;

		try {
			while (!(request = in.readLine()).equalsIgnoreCase("EXIT")) {
				System.out.println("Requested command: " + request);
				
				if (request.length() > 0) {
					Response response = user.processCommand(request);
					out.println(gson.toJson(response) + "\r\n");
				}
			}
			
			// Close all connections
			in.close();
			out.close();
			socket.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
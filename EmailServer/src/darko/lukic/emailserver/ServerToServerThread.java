package darko.lukic.emailserver;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;

import com.google.gson.Gson;

import darko.lukic.emailserver.controllers.UserController;
import darko.lukic.emailshared.Config;
import darko.lukic.emailshared.models.Response;

/**
 * Optimized communication protocol to communicate with a server
 * 
 * @author lukicdarkoo
 *
 */
public class ServerToServerThread extends Thread {
	/**
	 * Server can communicate with multiple users therefore all of them have unique ID
	 */
	private HashMap<String, UserController> userControllers = new HashMap<>();
	private BufferedReader in;
	private PrintWriter out;
	private Gson gson = new Gson();
	private ServerSocket ss;
	
	public ServerToServerThread() {
		start();
	}
	
	public void run() {
		try {
			ss = new ServerSocket(Config.SERVER2SERVER_PORT);
			acceptConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void acceptConnection() {
		String request;
		Socket socket;
		
		try {
			socket = ss.accept();
			
			System.out.println("S2S: Client connected");

			// Initialize input stream
			in = new BufferedReader(
					new InputStreamReader(socket.getInputStream()));
	
			// Initialize output stream
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream())), true);
			
			while ((request = in.readLine()) != null && request.equalsIgnoreCase("EXIT") == false) {
				if (request.length() == 0) {
					continue;
				}
				
				String[] requestTokens = request.split(" ");
				String token = requestTokens[0];
				String command = request.substring(request.indexOf(" ") + 1);
				
				System.out.println("S2S: Requested command: " + command);
				
				if (userControllers.containsKey(requestTokens[0]) == false) {
					userControllers.put(token, new UserController());
				}
				Response response = userControllers.get(token).processCommand(command);
				out.println(gson.toJson(response) + "\r\n");
			}
		} catch (IOException e) {
			
		}
		
		System.out.println("Restarting server to server communication...");
		try {
			in.close();
			out.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		acceptConnection();
	}
}

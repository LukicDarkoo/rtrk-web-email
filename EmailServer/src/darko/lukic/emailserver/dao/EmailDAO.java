package darko.lukic.emailserver.dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import darko.lukic.emailshared.models.Email;

/**
 * Data access object for emails
 * @author lukicdarkoo
 */
public class EmailDAO {
	private static final String FILE_NAME = "emails.txt";
	private static ArrayList<Email> emails = new ArrayList<Email>();
	private static Gson gson = new Gson();
	private static int lastEmailId = 0;
	
	/**
	 * Initialize email data access object
	 * @throws IOException
	 */
	public static void init() throws IOException {
		load();
	}
	
	/**
	 * Find all emails
	 * @return Array of emails
	 */
	public static ArrayList<Email> findAll() {
		return new ArrayList<Email>(emails);
	}
	
	/**
	 * Find emails by sender (`from` field in email)
	 * @param username - Username of sender
	 * @return List of emails
	 */
	public static ArrayList<Email> findBySender(String username) {
		ArrayList<Email> filteredEmails = new ArrayList<Email>();
		for (Email email : emails) {
			if (email.getFrom().equals(username)) {
				filteredEmails.add(email);
			}
		}
		return filteredEmails;
	}
	
	/**
	 * Find email by providing it's ID
	 * @param id - ID of email
	 * @return Email
	 */
	public static Email findById(int id) {
		for (Email email : emails) {
			if (email.getId() == id) {
				return email;
			}
		}
		return null;
	} 
	
	/**
	 * Find emails by receiver (`to` field in email)
	 * @param username - Username of receiver
	 * @return List of emails
	 */
	public static ArrayList<Email> findByReceiver(String username) {
		ArrayList<Email> filteredEmails = new ArrayList<Email>();
		for (Email email : emails) {
			if (email.getTo().equals(username)) {
				filteredEmails.add(email);
			}
		}
		return filteredEmails;
	}
	
	/**
	 * Add an email to data access object
	 * @param email Email to be added
	 */
	public static void add(Email email) {
		email.setId(++lastEmailId);
		emails.add(email);
		
		try {
			save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load data from file to memory
	 * @throws IOException
	 */
	private static void load() throws IOException {
		synchronized (emails) {
			Reader fileReader = new FileReader(FILE_NAME);
			emails = gson.fromJson(fileReader, new TypeToken<List<Email>>(){}.getType());
			lastEmailId = emails.get(emails.size() - 1).getId();
			fileReader.close();
		}
	}
	
	/**
	 * Load data from memory to file
	 * @throws IOException
	 */
	private static void save() throws IOException  {
		synchronized (emails) {
			Writer writer = new FileWriter(FILE_NAME);
		    gson.toJson(emails, writer);
		    writer.close();
		}
	}
}

package darko.lukic.emailserver.dao;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import darko.lukic.emailshared.models.Email;
import darko.lukic.emailshared.models.User;

/**
 * Data access object for users
 * @author lukicdarkoo
 */
public class UserDAO {
	private static final String FILE_NAME = "users.txt";
	private static ArrayList<User> users = new ArrayList<User>();
	private static Gson gson = new Gson();
	
	/**
	 * Find all available users
	 * @return List of all users
	 */
	public static ArrayList<User> findAll() {
		return new ArrayList<User>(users);
	}
	
	/**
	 * Initialize user data access object
	 * @throws IOException
	 */
	public static void init() throws IOException {
		load();
	}
	
	/**
	 * Find user by username
	 * @param username User's username
	 * @return User or `null` if user is not found
	 */
	public static User findByUsername(String username) {
		for (User user : users) {
			if (user.getUsername().equals(username)) {
				return user;
			}
		}
		return null;
	}
	
	/**
	 * Add use to data access object
	 * @param user User to be added
	 */
	public static void add(User user) {
		users.add(user);
		
		try {
			save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load data from file to memory
	 * @throws IOException
	 */
	private static void load() throws IOException {
		synchronized (users) {
			Reader fileReader = new FileReader(FILE_NAME);
			users = gson.fromJson(fileReader, new TypeToken<List<User>>(){}.getType());
			fileReader.close();
		}
	}
	
	/**
	 * Load data from memory to file
	 * @throws IOException
	 */
	private static void save() throws IOException  {
		synchronized (users) {
			Writer writer = new FileWriter(FILE_NAME);
		    gson.toJson(users, writer);
		    writer.close();
		}
	}
}

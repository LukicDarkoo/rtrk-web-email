package darko.lukic.emailserver;

import java.net.ServerSocket;
import java.net.Socket;

import darko.lukic.emailserver.dao.EmailDAO;
import darko.lukic.emailserver.dao.UserDAO;
import darko.lukic.emailshared.Config;

/**
 * Bootstraping class for EmailServer
 * @author lukicdarkoo
 *
 */
public class Server {
	public static void main(String[] args) {
		try {
			EmailDAO.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			UserDAO.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		new ServerToServerThread();
		
		try {
			// Listen for requests on given port
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(Config.SERVER2CLIENT_PORT);

			System.out.println("Server running...");
			while (true) {
				Socket sock = ss.accept();
				new ServerToClientThread(sock);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

package darko.lukic.emailserver.controllers;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import darko.lukic.emailshared.models.Response;
import darko.lukic.emailshared.models.ResponseCode;

public class UserControllerTest {
	private UserController userController;
	
	@Before
	public void setUp() throws Exception {
		userController = new UserController();
	}

	@Test
	public void testSendBeforeLogin() {
		Response response = userController.processCommand("SEND lukicdarkoo \"subject\" \"content content\"");
		assert(response.getCode().equals(ResponseCode.NOT_LOGGED_IN) == true);
	}
	
	@Test
	public void testRegister() {
		Response response = userController.processCommand("REGISTER testuser 123 Darko");
		assert(response.getCode() == ResponseCode.PASSWORD_SHORT);
		
		response = userController.processCommand("REGISTER testuser 12345 Darko");
		assert(
				response.getCode() == ResponseCode.SUCCESS ||
				response.getCode() == ResponseCode.USERNAME_ALREADY_EXISTS);
		
		response = userController.processCommand("LOGOFF");
		assert(response.getCode() == ResponseCode.SUCCESS);
	}
	
	@Test
	public void testLoginAndLogoff() {
		Response response = userController.processCommand("LOGIN testuser 12345");
		assert(response.getCode() == ResponseCode.SUCCESS);
		
		response = userController.processCommand("LOGOFF");
		assert(response.getCode() == ResponseCode.SUCCESS);
	}
	
}

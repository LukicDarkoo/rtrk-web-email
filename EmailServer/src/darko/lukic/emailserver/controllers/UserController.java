package darko.lukic.emailserver.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import darko.lukic.emailserver.dao.EmailDAO;
import darko.lukic.emailserver.dao.UserDAO;
import darko.lukic.emailshared.Utils;
import darko.lukic.emailshared.models.Email;
import darko.lukic.emailshared.models.Response;
import darko.lukic.emailshared.models.ResponseCode;
import darko.lukic.emailshared.models.User;

/**
 * Controls user's session and provides basic logic
 * @author lukicdarkoo
 *
 */
public class UserController {
	/**
	 * Reference to logged in user
	 */
	private User user = null;
	
	/**
	 * Instance of Gson JSON parser
	 */
	private Gson gson = new Gson();

	/**
	 * Process command received from client
	 * @param request Command received from client
	 * @return Response to client
	 */
	public Response processCommand(String request) {
		Response response = null;
		List<String> requestTokens = new ArrayList<String>();
		Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(request);
		while (m.find()) {
			requestTokens.add(m.group(1));
		}
		
		switch (requestTokens.get(0).toUpperCase()) {
		case "REGISTER":
			response = processRegister(requestTokens);
			break;
		case "LOGIN":
			response = processLogin(requestTokens);
			break;
		case "LOGOFF":
			response = processLogoff(requestTokens);
			break;
		case "LIST":
			response = processList(requestTokens);
			break;
		case "RECEIVE":
			response = processReceive(requestTokens);
			break;
		case "SEND":
			response = processSend(requestTokens);
			break;
		default:
			String responseText = "Invalid command\r\n";
			responseText += "Valid commands are: REGISTER, LOGIN, LIST, RECEIVE & SEND";
			response = new Response(ResponseCode.UNDEFINED_COMMAND, responseText);
			break;
		}
		return response;
	}
	
	/**
	 * Process REGISTER command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processRegister(List<String> args) {
		if (args.size() != 4) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, REGISTER <username> <password> <name>"
					);
		}
		
		String username = args.get(1);
		String password = args.get(2);
		String name = args.get(3);
		
		// TODO: Add more checks
		if (user != null) {
			return new Response(ResponseCode.ALREADY_LOGGED_IN, "You are already logged in");
		}
		else if (username.length() < 4) {
			return new Response(ResponseCode.USERNAME_SHORT, "Username is too short");
		} 
		else if (name.length() < 4) {
			return new Response(ResponseCode.NAME_SHORT, "Name is too short");
		} 
		else if (password.length() < 4) {
			return new Response(ResponseCode.PASSWORD_SHORT, "Password is too short");
		}
		else if (username.length() > 10) {
			return new Response(ResponseCode.USERNAME_LONG, "Username is too long");
		} 
		else if (name.length() > 10) {
			return new Response(ResponseCode.NAME_LONG, "Name is too long");
		} 
		else if (password.length() > 10) {
			return new Response(ResponseCode.PASSWORD_LONG, "Password is too long");
		}
		else if (UserDAO.findByUsername(username) != null) {
			return new Response(ResponseCode.USERNAME_ALREADY_EXISTS, "Username already exists try another");
		} 
		
		// Low security, better crypting algorithm should be used with salt
		User newUser = new User();
		newUser.setUsername(username);
		newUser.setPassword(DigestUtils.md5Hex(password));
		newUser.setName(name);
		UserDAO.add(newUser);
		user = newUser;
		
		return new Response(ResponseCode.SUCCESS, "User is succefully created");
	}
	
	/**
	 * Process LOGIN command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processLogin(List<String> args) {
		if (args.size() != 3) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, LOGIN <username> <password>"
					);
		}
		
		String username = args.get(1);
		String password = args.get(2);
		User newUser = null;
		
		if (user != null) {
			return new Response(ResponseCode.ALREADY_LOGGED_IN, "You are already logged in");
		}
		else if ((newUser = UserDAO.findByUsername(username)) == null) {
			return new Response(ResponseCode.NO_USER_FOUND, "No user found with given username");
		} 
		
		if (newUser.getPassword().equals(DigestUtils.md5Hex(password)) == false) {
			return new Response(ResponseCode.INCORRECT_PASSWORD, "Incorrect password for given username");
		} 
		
		user = newUser;
				
		return new Response(ResponseCode.SUCCESS, "User successfully logged");
	}
	
	/**
	 * Process LOGOFF command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processLogoff(List<String> args) {
		if (args.size() != 1) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, LOGOFF"
					);
		}
		
		if (user == null) {
			return new Response(
					ResponseCode.NOT_LOGGED_IN, 
					"You are not logged in"
					);
		}
		
		user = null;
		
		return new Response(ResponseCode.SUCCESS, "User successfully logged off");
	}
	
	/**
	 * Process LIST command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processList(List<String> args) {
		if (args.size() != 1) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, LIST"
					);
		}
		
		if (user == null) {
			return new Response(
					ResponseCode.NOT_LOGGED_IN, 
					"You are not logged in"
					);
		}
		
		Gson filteredGson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		return new Response(
				ResponseCode.SUCCESS, 
				filteredGson.toJson(EmailDAO.findByReceiver(user.getUsername()))
				);
	}
	
	/**
	 * Process RECEIVE command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processReceive(List<String> args) {
		if (args.size() != 2 || Utils.isInteger(args.get(1)) == false) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, RECEIVE <message_id>"
					);
		}
		
		int id = Integer.parseInt(args.get(1));
		Email email = null;
		
		if (user == null) {
			return new Response(
					ResponseCode.NOT_LOGGED_IN, 
					"You are not logged in"
					);
		} 
		else if ((email = EmailDAO.findById(id)) == null) {
			return new Response(
					ResponseCode.EMAIL_NOT_FOUND, 
					"Email not found"
					);
		}
		else if (email.getTo().equals(user.getUsername()) == false) {
			return new Response(
					ResponseCode.NO_PERMISSION, 
					"No permision to view this email"
					);
		}
				
		return new Response(
				ResponseCode.SUCCESS, 
				gson.toJson(email)
				);
	}
	
	/**
	 * Process SEND command and generate response to client
	 * @param args Arguments from client's CLI
	 * @return Response for client
	 */
	private Response processSend(List<String> args) {
		if (args.size() != 4) {
			return new Response(
					ResponseCode.INVALID_COMMAND_USAGE, 
					"Invalid command usage, SEND <to> \"<subject>\" \"<content>\""
					);
		}
		
		if (user == null) {
			return new Response(
					ResponseCode.NOT_LOGGED_IN, 
					"You are not logged in"
					);
		} 

		String to = args.get(1);
		String subject = args.get(2).replaceAll("^\"|\"$", "");
		String text = args.get(3).replaceAll("^\"|\"$", "");
				
		Email email = new Email();
		email.setFrom(user.getUsername());
		email.setTo(to);
		email.setSubject(subject);
		email.setContent(text);
		EmailDAO.add(email);		
		
		return new Response(
				ResponseCode.SUCCESS, 
				"Message successfully sent"
				);
	}
}

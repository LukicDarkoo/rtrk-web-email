# Web Programming

## Getting started

### Clone repository
```
git clone https://bitbucket.org/LukicDarkoo/rtrk-web-email.git --depth=1
``` 

### Run from terminal
Eclipse's console is extremely bad, however I've built a Makefile therefor you can
compile and run from terminal: `make client` or `make server`.  

NOTE: Make sure you have installed *apache-ant* (`pacman -S apache-ant`).
